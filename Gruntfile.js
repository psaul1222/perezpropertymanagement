
module.exports = function(grunt) {


  grunt.initConfig({
pkg: grunt.file.readJSON('package.json'),
    // all of our configuration will go here

    jshint: {
    files:['Gruntfile.js','app/../app/modules/**/controllers/*.js','app/../app/modules/**/services/*.js'],
     options: {
       reporter: require('jshint-stylish')
     }
   },
   ngAnnotate: {
       options: {
           singleQuotes: true
       },
       app: {
           files: {
               'app/app.annotate.js': ['app/app.js','app/../app/modules/**/controllers/*.js','app/../app/modules/**/services/*.js'],
           }
       }
   },
   concat: {
    js: { //target
        src: 'app/app.annotate.js',
        dest:'app/app.concat.js'
    }
  },


   uglify: {
     options: {
       banner: ''
     },
     build: {
       files: {
         'app/app.min.js': 'app/app.concat.js'
       }
     }
   },

   clean : {
    buildProcess : {
        src : [ "app/app.concat.js",
                "app/app.annotate.js"
        ]
    }
  }

  });

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // we can only load these if they are in our package.json
  // make sure you have run npm install so our app can find these
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['jshint','ngAnnotate', 'concat', 'uglify']);

};
