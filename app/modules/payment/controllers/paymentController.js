angular.module('perezTMS.payment').config(function($stateProvider) {
    $stateProvider
    .state("paymentCreate", {
        url: "/payment/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/payment/views/create.html",
            controller:"createPayment"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PaymentMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("paymentEdit", {
        url: "/payment/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/payment/views/edit.html",
            controller:"editPayment"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PaymentMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("paymentList", {
        url: '/payment/',
        params:{ msg:'',action:'list'},
        views: {
          'body' : {
            templateUrl :  "../app/modules/payment/views/list.html",
            controller:"listPayment"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PaymentMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    }).state("paymentClear", {
        url: '/payment/clear',
        params:{ msg:'',action:'clear'},
        views: {
          'body' : {
            templateUrl :  "../app/modules/payment/views/list.html",
            controller:"listPayment"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PaymentMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.payment').controller("createPayment", function ($scope,Payment,Lease,PaymentType,$filter) {
    $scope.paymentTypes = PaymentType.query();
    $scope.leases = Lease.query();
    $scope.payment = new Payment();
    $scope.payment.date = new Date();
    $scope.createPayment = function() {
    $scope.payment.date = $filter('date')($scope.payment.date, 'yyyy/MM/dd');
      $scope.payment.$save(function(response) {
        $scope.msg = response.message;
        $state.go('paymentList');
     });
 };

});

angular.module('perezTMS.payment').controller("editPayment", function ($scope) {
    $scope.contrler = "payment";
});

angular.module('perezTMS.payment').controller("listPayment", function ($scope,Payment,$stateParams,$state) {

    $scope.msg = $stateParams.msg;
    $scope.action = $stateParams.action;

    $scope.payments = Payment.query();

  $scope.clearPayments = function(){

    var numChanged=0;
    for(var i =0; i < $scope.payments.length; i++)
    {
      var paym = $scope.payments[i];
      if(paym.changed)
      {
        numChanged++;
        paym.$clear();
      }

    }
    if(numChanged>0){
      $state.go('paymentList');
    }
    else{
      $scope.msg="No Changes made";
    }

  };


    $scope.deletePayment = function(payment){


      if (confirm("Are you sure you want to delete "+payment.amount +" on "+payment.created_at +" for "+payment.lease.rental_unit.address.street +" "+( !payment.lease.rental_unit.address.apartment ? "": " Apt "+payment.lease.rental_unit.address.apartment+" ")+"?"  )) {
        payment.$delete(function(response) {
          $scope.msg = response.message;
          var index = $scope.payments.indexOf(payment);
          $scope.payments.splice(index, 1);
       });
     }
   };

});
angular.module('perezTMS.payment').controller("PaymentMenu",function($scope){
  $scope.contrler = "payment";
  $scope.menu = ['Create','Clear','List'];
});
