angular.module('perezTMS.payment').factory('Payment', ['$resource',function($resource) {
  return $resource(hostApi+'/api/payment/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []},
    delete: {method: 'DELETE', transformResponse: []},
    clear: {method: 'PUT', transformResponse: [],url:hostApi+'/api/payment/clear/:id'}
  });

}]);
angular.module('perezTMS.payment').factory('PaymentType', ['$resource',function($resource) {
  return $resource(hostApi+'/api/paymentType/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []},
    delete: {method: 'DELETE', transformResponse: []}
  });

}]);
