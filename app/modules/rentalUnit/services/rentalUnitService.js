angular.module('perezTMS.rentalunit').factory('RentalUnit', ['$resource',function($resource) {
  return $resource(hostApi+'/api/rentalUnit/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });

}]);
