angular.module('perezTMS.rentalunit').config(function($stateProvider) {
    $stateProvider
    .state("rentalUnitCreateBuilding", {
        url: "/rentalUnit/createBuilding",
        views: {
          'body' : {
            templateUrl :  "../app/modules/rentalUnit/views/createBuilding.html",
            controller:"createBuildingRentalUnit"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "rentalUnitMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("rentalUnitCreate", {
        url: "/rentalUnit/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/rentalUnit/views/create.html",
            controller:"createRentalUnit"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "rentalUnitMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("rentalUnitEdit", {
        url: "/rentalUnit/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/rentalUnit/views/edit.html",
            controller:"editrentalUnit"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "rentalUnitMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("rentalUnitList", {
        url: '/rentalUnit/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/rentalUnit/views/list.html",
            controller:"listrentalUnit"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "rentalUnitMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.rentalunit').controller("createRentalUnit", function ($scope,RentalUnit,Property) {

    $scope.rentalUnit = new RentalUnit();
    $scope.rentalUnit.property = null;
    $scope.properties = Property.query();
    $scope.createRentalUnit = function() {
    if($scope.rentalUnit.property !== null && $scope.rentalUnit.property.id !== null)
      $scope.rentalUnit.property_id = $scope.rentalUnit.property.id;

    $scope.rentalUnit.$save(function(response) {
      $scope.msg = response.message;
     });
   };
 $scope.updateRUAddress = function(property){
   $scope.rentalUnit.address = property.address;
 };

});

angular.module('perezTMS.rentalunit').controller("createBuildingRentalUnit", function ($scope,RentalUnit,Property,$state) {

    $scope.rentalUnit = new RentalUnit();
    $scope.rentalUnit.property = null;
    $scope.properties = Property.query();
    $scope.floors = 0;
    $scope.building= {floors:[]};
    $scope.updateRUAddress = function(property){
      $scope.rentalUnit.address = property.address;
    };
    $scope.getBuildingFloors = function(num) {
      num = Math.abs(num);

      if( $scope.building.floors.length > num)
      {
        num = (num-1 <0 )? 0 : num;
        $scope.building.floors.splice(num,$scope.building.floors.length-num);
      }
      else {
        for(var i =$scope.building.floors.length; i< num; i++){
          $scope.building.floors.push([]);
        }
      }

    };
    $scope.formatThFloor = function(floor){
      floor = floor + 1;
      switch (floor){
        case 1: return "1st";
        case 2: return "2nd";
        case 3: return "3rd";
        default: return floor +"th" ;
      }
    };

    $scope.createBuildingRentalUnits = function() {


    if($scope.rentalUnit.property !== null && $scope.rentalUnit.property.id !== null){

      $scope.rentalUnit.property_id = $scope.rentalUnit.property.id;

      $scope.Build(0,null,null);

      }

   };
   $scope.Build = function(floorIndex,floor,currentApt){

     if(floorIndex> ($scope.building.floors.length-1)  )
     {
       return -1;
     }
     //new floor
     if(floor=== null)
     {
       floor = $scope.building.floors[floorIndex];
       currentApt = floor.startApt;
     }
     else if(currentApt>floor.endApt)
     {
       return $scope.Build(floorIndex+1,null,null);
     }

     var tempRentalUnit = new RentalUnit();
     tempRentalUnit.property_id = $scope.rentalUnit.property_id;
     tempRentalUnit.address = $scope.rentalUnit.address;
     tempRentalUnit.address.apartment = currentApt;
     tempRentalUnit.$save(function(){
       var result = $scope.Build(floorIndex,floor,currentApt+floor.increment);
       if(result <0) $state.go('rentalUnitList');

     });
   };

});

angular.module('perezTMS.rentalunit').controller("rentalUnitMenu",function($scope){
  $scope.contrler = "rentalUnit";
  $scope.menu = ['Create','CreateBuilding','List'];
});

angular.module('perezTMS.rentalunit').controller("editrentalUnit", function ($scope) {
    $scope.contrler = "rentalUnit";
});

angular.module('perezTMS.rentalunit').controller("listrentalUnit", function ($scope,RentalUnit) {
    $scope.rentalUnits = RentalUnit.query();

    $scope.deleteRU = function(ru){

      if (confirm("Are you sure you want to delete "+ru.name)) {
        ru.$delete(function(response) {
          $scope.msg = response.message;
          var index = $scope.rentalUnits.indexOf(ru);
          $scope.rentalUnits.splice(index, 1);
       });
     }
   };

});
