angular.module('perezTMS.menubar').config(function($stateProvider) {
    $stateProvider
    .state("home", {
        url: "/home",
        views: {
          'body' : {
            templateUrl :  "../app/modules/lease/views/create.html",
            controller:"createlease"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "leaseMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
  });




angular.module('perezTMS.menubar').controller("menuController", function ($rootScope,$scope,$location,$auth,Access) {
 if($auth.isAuthenticated()){

   if($rootScope.controllers === undefined || $rootScope.controllers === null)
   {
     console.log("rootScope empty");
     $rootScope.controllers = Access.query();
     $rootScope.controllers.$promise.then(function(result){
       $rootScope.controllers = result;
       $scope.controllers = $rootScope.controllers.pages;
     });
   }
   else {
     $scope.controllers = $rootScope.controllers.pages;
   }


//  $scope.controllers = ['property','rentalUnit','tenant','lease','payment','user','logout'];
}
else {
  $scope.controllers =['login'];
}

  $scope.formatTitle = function(title)
  {
    switch(title){
      case "rentalUnit": return "Rental Unit";
      default: return title.charAt(0).toUpperCase() + title.slice(1);
    }
  };
  $scope.formatState = function(state)
  {
    switch(state){
      case 'logout' :
      case "login": return state;
      default: return state+"List";
    }
  };

});
