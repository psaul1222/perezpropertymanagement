angular.module('perezTMS.admin').factory('Appliance', ['$resource',function($resource) {
  return $resource(hostApi+'/api/appliance/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });
}]);

angular.module('perezTMS.admin').factory('ApplianceType', ['$resource',function($resource) {
  return $resource(hostApi+'/api/applianceType/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });
}]);

angular.module('perezTMS.admin').factory('ApplianceModel', ['$resource',function($resource) {
  return $resource(hostApi+'/api/applianceModel/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });
}]);

angular.module('perezTMS.admin').factory('ApplianceManufacturer', ['$resource',function($resource) {
  return $resource(hostApi+'/api/applianceManufacturer/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });
}]);
