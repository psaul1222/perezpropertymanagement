angular.module('perezTMS.admin').factory('SystemSetting', ['$resource',function($resource) {
  return $resource(hostApi+'/api/setting/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []},
    update: {method: 'PUT'}
  });
}]);
