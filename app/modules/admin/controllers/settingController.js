angular.module('perezTMS.admin').config(function($stateProvider) {
    $stateProvider
    .state("adminSettings", {
        url: "/admin/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/admin/views/settings.html",
            controller:"settingsAdmin"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "AdminMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
  });

  angular.module('perezTMS.admin').controller("settingsAdmin", function ($scope,SystemSetting) {

   $scope.systemSettings = SystemSetting.query();
   $scope.edit = {'mode':false,'index':null};

   $scope.setting = new SystemSetting();
   $scope.createSystemSetting = function() {
     if( $scope.edit.mode)
     {
       $scope.setting.$update(function(response) {
         if(response.status_code<0)
         {
           $scope.error = response.message;
         }
         else{
           $scope.error = null;
           $scope.systemSettings[$scope.edit.index] = new SystemSetting(response.data);
           $scope.edit.mode = false;
           $scope.edit.index = null;
         }
      });
     }
     else {
       $scope.setting.$save(function(response) {
         if(response.status_code<0)
         {
           $scope.error = response.message;
         }
         else{
           $scope.error = null;
           $scope.systemSettings.push(new SystemSetting(response.data));
         }
      });
     }

   };
   $scope.deleteSystemSetting = function(setting){
     if (confirm("Are you sure you want to delete "+setting.type+" ?")) {
       setting.$delete(function(response) {

         if(response.status_code<0)
         {
           $scope.error = response.message;
         }
         else{
           $scope.error = null;
           var index = $scope.systemSettings.indexOf(setting);
           $scope.systemSettings.splice(index, 1);
         }
      });
    }
  };
  $scope.editSystemSetting = function(setting){
    var index = $scope.systemSettings.indexOf(setting);
    $scope.setting = setting;
    $scope.edit.index = index;
    $scope.edit.mode = true;
     };


  });
