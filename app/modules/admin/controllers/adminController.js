angular.module('perezTMS.admin').config(function($stateProvider) {
    $stateProvider
    .state("adminCreate", {
        url: "/admin/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/admin/views/create.html",
            controller:"createAdmin"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "AdminMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("adminAppliances", {
        url: "/admin/appliances",
        views: {
          'body' : {
            templateUrl :  "../app/modules/admin/views/appliances.html",
            controller:"appliancesAdmin"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "AdminMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("adminList", {
        url: '/admin/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/admin/views/list.html",
            controller:"listAdmin"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "AdminMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.admin').controller("createAdmin", function ($scope,$location) {

 //    $scope.admin = new Admin();
 //    $scope.createAdmin = function() {
 //    $scope.admin.$save(function(response) {
 //      $scope.msg = response.message;
 //   });
 // };

});

angular.module('perezTMS.admin').controller("appliancesAdmin", function ($scope,Appliance,ApplianceModel,ApplianceManufacturer,ApplianceType,$location) {

  $scope.applianceManufacturers = ApplianceManufacturer.query();
  $scope.applianceTypes = ApplianceType.query();
  $scope.applianceModels = ApplianceModel.query();
  $scope.appliances = Appliance.query();
  $scope.manufacturer = new ApplianceManufacturer();

  $scope.createManufacturer = function() {
    $scope.manufacturer.$save(function(response) {
      if(response.status_code<0)
      {
        $scope.error = response.message;
      }
      else{
        $scope.error = null;
        $scope.applianceManufacturers.push(new ApplianceManufacturer(response.data));
      }
   });
  };
  $scope.deleteManufacturer= function(manufacturer){
    if (confirm("Are you sure you want to delete "+manufacturer.name+" ?")) {
      manufacturer.$delete(function(response) {

        if(response.status_code<0)
        {
          $scope.error = response.message;
        }
        else{
          $scope.error = null;
          var index = $scope.applianceManufacturers.indexOf(manufacturer);
          $scope.applianceManufacturers.splice(index, 1);
        }
     });
   }
 };
$scope.applianceType = new ApplianceType();
 $scope.createApplianceType = function() {
   $scope.applianceType.$save(function(response) {
     if(response.status_code<0)
     {
       $scope.error = response.message;
     }
     else{
       $scope.error = null;
       $scope.applianceTypes.push(new ApplianceType(response.data));
     }
  });
 };
 $scope.deleteApplianceType= function(type){
   if (confirm("Are you sure you want to delete "+type.type+" ?")) {
     type.$delete(function(response) {

       if(response.status_code<0)
       {
         $scope.error = response.message;
       }
       else{
         $scope.error = null;
         var index = $scope.applianceTypes.indexOf(type);
         $scope.applianceTypes.splice(index, 1);
       }
    });
  }
};

$scope.applianceModel = new ApplianceModel();
 $scope.createApplianceModel = function() {
   $scope.applianceModel.$save(function(response) {
     if(response.status_code<0)
     {
       $scope.error = response.message;
     }
     else{
       $scope.error = null;
       $scope.applianceModels.push(new ApplianceModel(response.data));
     }
  });
 };
 $scope.deleteApplianceModel= function(model){
   if (confirm("Are you sure you want to delete "+model.name+" ?")) {
     model.$delete(function(response) {

       if(response.status_code<0)
       {
         $scope.error = response.message;
       }
       else{
         $scope.error = null;
         var index = $scope.applianceModels.indexOf(model);
         $scope.applianceModels.splice(index, 1);
       }
    });
  }
};

$scope.appliance = new Appliance();
 $scope.createAppliance = function() {
   $scope.appliance.$save(function(response) {
     if(response.status_code<0)
     {
       $scope.error = response.message;
     }
     else{
       $scope.error = null;
       $scope.appliances.push(new Appliance(response.data));
     }
  });
 };
 $scope.deleteAppliance= function(appliance){
   if (confirm("Are you sure you want to delete "+appliance.appliance_type.type+" "+appliance.appliance_manufacturer.name+" "+appliance.appliance_model.name+" ?")) {
     model.$delete(function(response) {

       if(response.status_code<0)
       {
         $scope.error = response.message;
       }
       else{
         $scope.error = null;
         var index = $scope.appliances.indexOf(appliance);
         $scope.applianceMs.splice(index, 1);
       }
    });
  }
};
});

angular.module('perezTMS.admin').controller("editAdmin", function ($scope) {
    $scope.contrler = "admin";
});

angular.module('perezTMS.admin').controller("listAdmin", function ($scope) {


});
angular.module('perezTMS.admin').controller("AdminMenu",function($scope){
  $scope.contrler = "admin";
  $scope.menu = ['Appliances','Settings','List'];
});
