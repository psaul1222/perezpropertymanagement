angular.module('perezTMS.user').config(function($stateProvider) {
    $stateProvider
    .state("userCreate", {
        url: "/user/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/user/views/create.html",
            controller:"createUser"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "UserMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("userEdit", {
        url: "/user/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/user/views/edit.html",
            controller:"editUser"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "UserMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("userList", {
        url: '/user/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/user/views/list.html",
            controller:"listUser"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "UserMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.user').controller("createUser", function ($scope,User,$location) {

    $scope.user = new User();
    $scope.createUser = function() {
    $scope.user.$save(function(response) {
      $scope.msg = response.message;
   });
 };

});

angular.module('perezTMS.user').controller("editUser", function ($scope) {
    $scope.contrler = "user";
});

angular.module('perezTMS.user').controller("listUser", function ($scope,User) {
    $scope.users = User.query();

    $scope.deleteUser = function(user){


      if (confirm("Are you sure you want to delete "+user.name+" ?")) {
        user.$delete(function(response) {
          $scope.msg = response.message;
          if(response.status_code>0)
          {
            var index = $scope.users.indexOf(user);
            $scope.users.splice(index, 1);
          }
          else
          {
              user.name = response.data.name;
              user.admin = response.data.admin;
              user.manager = response.data.manager;
              user.email = response.data.email;
              user.id = response.data.id;
          }
       });
     }
   };

});
angular.module('perezTMS.user').controller("UserMenu",function($scope){
  $scope.contrler = "user";
  $scope.menu = ['Create','List'];
});
