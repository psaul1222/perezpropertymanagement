angular.module('perezTMS.user').factory('User', ['$resource',function($resource) {
  return $resource(hostApi+'/api/user/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });

}]);
