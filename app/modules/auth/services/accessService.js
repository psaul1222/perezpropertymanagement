angular.module('perezTMS.auth').factory('Access', ['$resource',function($resource) {
  return $resource(hostApi+'/api/access/:id',{id:'@id'}, {
    'query': {method: 'GET', isArray: false, interceptor: 'AuthInterceptor'}
  });
}]);
