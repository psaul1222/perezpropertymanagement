angular.module('perezTMS.auth').run(function($transitions,$auth) {
  $transitions.onStart({ to: '**' }, function(trans) {
    var auth = trans.injector().get('$auth');
    if (!$auth.isAuthenticated() && trans._targetState._identifier !== 'login') {
      // User isn't authenticated. Redirect to a new Target State
      var msg = "Log in first.";
      return trans.router.stateService.target('login');
    }
  });
});
