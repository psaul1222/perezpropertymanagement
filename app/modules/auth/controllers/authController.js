angular.module('perezTMS.auth').config(function($stateProvider, $urlRouterProvider, $authProvider) {

  $authProvider.loginUrl = hostApi+'/api/auth';
  $urlRouterProvider.otherwise('/login');

    $stateProvider
    .state("login", {
        url: "/login",
        params:{ msg:'',action:''},
        views: {
          'body' : {
            templateUrl :  "../app/modules/auth/views/login.html",
            controller:"authLoginController as auth"
          },
          'menu' : {
            templateUrl :"../app/modules/auth/views/loggedOutMenu.html"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("logout", {
        url: "/logout",
        params:{ msg:'You have been logged out.', action:'logout'},
        views: {
          'body' : {
            templateUrl :  "../app/modules/auth/views/login.html",
            controller:"authLoginController as auth"
          },
          'menu' : {
            templateUrl :"../app/modules/auth/views/loggedOutMenu.html"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
  });

  angular.module('perezTMS.auth').controller("authLoginController", function ($rootScope,$scope,$state,$auth,$window,$stateParams,Access) {

    $scope.msg = $stateParams.msg;

    if($stateParams.action === 'logout')
    {
      $rootScope.controllers = null;
       $auth.logout();
       $state.go('login', {'msg':"You have been logged out."});
    }
    $scope.email = null;
    $scope.password = null;

    $scope.login = function (){


      if($scope.email !== null && $scope.password !== null)
      {
        var postData = {
            grant_type: "password",
            email: $scope.email,
            password: $scope.password,
            scope: ""
        };


        $auth.login(postData).then(function(response) {
          if(response.data !== undefined && response.data.token !== undefined){
            $auth.setToken(response.data.token);
            $rootScope.controllers = Access.query();
            $rootScope.controllers.$promise.then(function(result){
              $rootScope.controllers = result;
              $state.go('home', {});
            });

          }
          else {
            $scope.msg = response.data.error;
          }
          // $window.sessionStorage.setItem('token', response.data.token);


        },function(response)
        {
          console.log(response);
        });
      }
      else {
        $scope.msg = "Enter username and password.";
      }
    };

});
