angular.module('perezTMS.tenant').factory('Tenant', ['$resource',function($resource) {
  return $resource(hostApi+'/api/tenant/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []}
  });

}]);
