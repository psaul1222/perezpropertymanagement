angular.module('perezTMS.tenant').config(function($stateProvider) {
    $stateProvider
    .state("tenantCreate", {
        url: "/tenant/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/tenant/views/create.html",
            controller:"createTenant"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "TenantMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("tenantEdit", {
        url: "/tenant/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/tenant/views/edit.html",
            controller:"editTenant"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "TenantMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("tenantList", {
        url: '/tenant/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/tenant/views/list.html",
            controller:"listTenant"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "TenantMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.tenant').controller("createTenant", function ($scope,Tenant,$location) {

    $scope.tenant = new Tenant();
    $scope.createTenant = function() {
    $scope.tenant.$save(function(response) {
      $scope.msg = response.message;
   });
 };

});

angular.module('perezTMS.tenant').controller("editTenant", function ($scope) {
    $scope.contrler = "tenant";
});

angular.module('perezTMS.tenant').controller("listTenant", function ($scope,Tenant) {
    $scope.tenants = Tenant.query();

    $scope.deleteTenant = function(tenant){


      if (confirm("Are you sure you want to delete "+tenant.first_name+" "+tenant.last_name)) {
        tenant.$delete(function(response) {
          $scope.msg = response.message;
          var index = $scope.tenants.indexOf(tenant);
          $scope.tenants.splice(index, 1);
       });
     }
   };

});
angular.module('perezTMS.tenant').controller("TenantMenu",function($scope){
  $scope.contrler = "tenant";
  $scope.menu = ['Create','List'];
});
