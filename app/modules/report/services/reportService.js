angular.module('perezTMS.report').factory('Report', ['$resource',function($resource) {
  return $resource(hostApi+'/api/report/generateRentalIncrease',{}, {
    rental_increase: {
    method: 'POST',
    headers: {
        accept: 'application/pdf'
    },
    responseType: 'arraybuffer',
    cache: true,
    transformResponse: function (data) {
        var pdf;
        if (data) {
            pdf = new Blob([data], {
                type: 'application/pdf'
            });
        }
        return {
            data: pdf
        };
        }
    }
  });

}]);
