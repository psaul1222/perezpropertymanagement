angular.module('perezTMS.report').config(function($stateProvider) {
    $stateProvider
    .state("reportRental_Increase", {
        url: "/report/Rental_Increase",
        views: {
          'body' : {
            templateUrl :  "../app/modules/report/views/create.html",
            controller:"Rental_Increase"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "reportMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("reportEdit", {
        url: "/report/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/report/views/edit.html",
            controller:"editreport"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "reportMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("reportList", {
        url: '/report/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/report/views/index.html",
            controller:"listReport"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "reportMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.report').controller("Rental_Increase", function ($scope,Report,$filter,$state) {

    $scope.report = new Report();

    $scope.createReport = function() {
      var fileName = "renttalIncrease_"+$scope.report.apt_number+".pdf";
      var rentIncrease = document.createElement("a");

    $scope.report.$rental_increase(function(response) {
      $scope.msg = response.message;
      var file = new Blob([response.data], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
      rentIncrease.href = fileURL;
      rentIncrease.download = fileName;
      rentIncrease.target='_blank';
      rentIncrease.click();
   });
 };
});
angular.module('perezTMS.report').controller("reportMenu",function($scope){
  $scope.contrler = "report";
  $scope.menu = ['Rental_Increase','List'];
});

angular.module('perezTMS.report').controller("editreport", function ($scope) {
    $scope.contrler = "report";
});

angular.module('perezTMS.report').controller("listReport", function ($scope,Report) {


});
