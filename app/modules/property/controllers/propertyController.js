angular.module('perezTMS.property').config(function($stateProvider) {
    $stateProvider
    .state("propertyCreate", {
        url: "/property/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/property/views/create.html",
            controller:"createProperty"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PropertyMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("propertyEdit", {
        url: "/property/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/property/views/edit.html",
            controller:"editProperty"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PropertyMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("propertyList", {
        url: '/property/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/property/views/list.html",
            controller:"listProperty"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "PropertyMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.property').controller("createProperty", function ($scope,Property,$location) {

    $scope.property = new Property();
    $scope.createProperty = function() {
    $scope.property.$save(function(response) {
      $scope.msg = response.message;
   });
 };

});

angular.module('perezTMS.property').controller("editProperty", function ($scope) {
    $scope.contrler = "property";
});

angular.module('perezTMS.property').controller("listProperty", function ($scope,Property) {
    $scope.properties = Property.query();

    $scope.deleteProperty = function(property){


      if (confirm("Are you sure you want to delete "+property.name)) {
        property.$delete(function(response) {
          $scope.msg = response.message;
          var index = $scope.properties.indexOf(property);
          $scope.properties.splice(index, 1);
       });
     }
   };

});
angular.module('perezTMS.property').controller("PropertyMenu",function($scope){
  $scope.contrler = "property";
  $scope.menu = ['Create','List'];
});
