angular.module('perezTMS.property').factory('Property', ['$resource',function($resource) {
  return $resource(hostApi+'/api/property/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false},
    add: {method: 'POST', transformResponse: []},
    delete: {method: 'DELETE', transformResponse: []}
  });

}]);
