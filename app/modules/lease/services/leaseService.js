angular.module('perezTMS.lease').factory('Lease', ['$resource',function($resource) {
  return $resource(hostApi+'/api/lease/:id',{id:'@id'}, {
    get: {method: 'GET', isArray: false, interceptor: 'AuthInterceptor'},
    add: {method: 'POST', transformResponse: [],interceptor: 'AuthInterceptor'}
  });

}]);

angular.module('perezTMS.lease').factory('LeasePDF', ['$resource',function($resource) {
  return $resource(hostApi+'/api/lease/:id/getPdf',{id:'@id'}, {
    getPdf: {
    method: 'GET',
    params:{id:'@id'},
    headers: {
        accept: 'application/pdf'
    },
    responseType: 'arraybuffer',
    cache: true,
    transformResponse: function (data) {
        var pdf;
        if (data) {
            pdf = new Blob([data], {
                type: 'application/pdf'
            });
        }
        return {
            data: pdf
        };
        }
    }
  });

}]);
