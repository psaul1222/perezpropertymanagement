angular.module('perezTMS.lease').config(function($stateProvider) {
    $stateProvider
    .state("leaseCreate", {
        url: "/lease/create",
        views: {
          'body' : {
            templateUrl :  "../app/modules/lease/views/create.html",
            controller:"createlease"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "leaseMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    })
    .state("leaseEdit", {
        url: "/lease/edit",
        views: {
          'body' : {
            templateUrl :  "../app/modules/lease/views/edit.html",
            controller:"editlease"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "leaseMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }

    })
    .state("leaseList", {
        url: '/lease/',
        views: {
          'body' : {
            templateUrl :  "../app/modules/lease/views/list.html",
            controller:"listLease"
          },
          'menu' : {
            templateUrl :"../app/modules/menubar/views/menuBar.html",
            controller: "leaseMenu"
          },
          'mainMenu' : {
            templateUrl :"../app/modules/menubar/views/mainMenu.html",
            controller:"menuController"
          }
        }
    });
});

angular.module('perezTMS.lease').controller("createlease", function ($scope,Lease,Tenant,RentalUnit,$filter,$state,ApplianceType) {

    $scope.lease = new Lease();
    $scope.applianceTypes = ApplianceType.query();
    //default values for a lease

    $scope.lease.return_check = 25;
    $scope.lease.late_charge = 25;
    $scope.lease.late_charge_days = 5;
    $scope.lease.term = 6;
    $scope.lease.start_date = new Date();

    $scope.lease.property = null;
    $scope.lease.rentalUnit = null;
    $scope.lease.tenant = null;
    $scope.lease.owed = null;
    $scope.rentalUnits = RentalUnit.query();
    $scope.tenants = Tenant.query();
    $scope.tenant = new Tenant();
    $scope.lease.appliance_types = [];

    $scope.createTenant = function() {
      $scope.tenant.$save(function(response) {
        $scope.msg = response.message;
        $scope.tenants.push(response.data);
        $scope.lease.tenant=response.data;
     });
    };

    $scope.createLease = function() {
    if($scope.lease.rentalUnit !== null && $scope.lease.rentalUnit.id !== null)
      $scope.lease.rental_unit_id = $scope.lease.rentalUnit.id;

    if($scope.lease.tenant !== null && $scope.lease.tenant.id !== null)
      $scope.lease.tenant_id = $scope.lease.tenant.id;

      $scope.lease.start_date_formated = $filter('date')($scope.lease.start_date, 'yyyy/MM/dd');

    $scope.lease.$save(function(response) {
      if(response.status_code >0)
      {
        $state.go('leaseList');
      }
      else {
          $scope.msg = response.message;
          response.data.start_date = new Date(response.data.start_date_formated);
          $scope.lease = new Lease(response.data);
      }

   });
 };
 $scope.$watch("lease.start_date", function(){
   var date = $scope.lease.start_date;
   var today = new Date();

   if(date.getYear()== today.getYear() && (date.getMonth() == today.getMonth() || (date.getMonth()== today.getMonth()+1 && date.getDate() <= today.getDate())    || (date.getMonth() == today.getMonth() -1 && date.getDate()>= today.getDate()) )    )
    {
      $scope.setCurrentlyOwed = false;
    }
    else {
      $scope.setCurrentlyOwed = true;
    }
 });
 $scope.addApplianceType = function (appliance)
 {
   $scope.lease.appliance_types.push(appliance);

 };
 $scope.removeLeaseApplianceType = function(type)
 {
   var index = $scope.lease.appliance_types.indexOf(type);
   $scope.lease.appliance_types.splice(index, 1);
 };


});

angular.module('perezTMS.lease').controller("leaseMenu",function($scope){
  $scope.contrler = "lease";
  $scope.menu = ['Create','List'];
});

angular.module('perezTMS.lease').controller("editlease", function ($scope) {
    $scope.contrler = "lease";
});

angular.module('perezTMS.lease').controller("listLease", function ($scope,Lease,LeasePDF) {
    $scope.leases = Lease.query();

    $scope.deleteLease = function(lease){

      if (confirm("Are you sure you want to delete "+lease.tenant.first_name+" "+lease.tenant.last_name+"'s Lease?")) {
        lease.$delete(function(response) {
          $scope.msg = response.message;
          var index = $scope.leases.indexOf(lease);
          $scope.leases.splice(index, 1);
       });
     }
   };
     $scope.downloadPDF = function(lease){
      var fileName = "lease_"+lease.id+".pdf";
      var leasePDF = document.createElement("a");
       LeasePDF.getPdf({id: lease.id},function(response){
         var file = new Blob([response.data], {type: 'application/pdf'});
         var fileURL = URL.createObjectURL(file);
         leasePDF.href = fileURL;
         leasePDF.download = fileName;
         leasePDF.target='_blank';
         leasePDF.click();
        // $scope.msg = response.message;
       });

     };

});
