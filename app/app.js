angular.module("perezTMS.admin", ["ngResource","ui.router"]);
angular.module("perezTMS.auth", ["ngResource","ui.router","satellizer"]);
angular.module("perezTMS.lease", ["ngResource","ui.router"]);
angular.module("perezTMS.menubar", ["ui.router"]);
angular.module("perezTMS.payment", ["ngResource","ui.router"]);
angular.module("perezTMS.property", ["ngResource","ui.router"]);
angular.module("perezTMS.rentalunit", ["ngResource","ui.router"]);
angular.module("perezTMS.report", ["ngResource","ui.router"]);
angular.module("perezTMS.tenant", ["ngResource","ui.router"]);
angular.module("perezTMS.user", ["ngResource","ui.router"]);


var app = angular.module("perezTMS", ["ui.router",
  "perezTMS.admin",
  "perezTMS.auth",
  "perezTMS.lease",
  "perezTMS.menubar",
  "perezTMS.payment",
  "perezTMS.property",
  "perezTMS.rentalunit",
  "perezTMS.report",
  "perezTMS.tenant",
  "perezTMS.user"
]);
var dev = true;
var hostApi = "http://192.168.1.81"
if(dev)
{
  hostApi = "http://localhost";
}
